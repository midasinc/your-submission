<?php
/*
 * Usage:
 * 
 * 	require_once('kitchenapi.class.php');
 *
 *	$client = new KitchenAPI_Client();
 *	$client->login();
 *	
 *	$client->searchByCategory();
 *  
 * 
 */
class KitchenAPI_Client
{
    const servername = 'api.campbellskitchen.com';
    var $_token_cache_fn = "";

	var $_userkey = 'entry21@hackthekitchen.com';
	var $_apikey = '29DCB1B2-7900-4B90-8DCC-438CB44103F9';
	var $_token = '';
	
	function __construct() {
    	$this->_token_cache_fn = "/tmp/" . KitchenAPI_Client::servername . ".token";
   	}
	

    public function getToken() {
    	return $this->_token;
    }

	private function _call($endpoint, $params = array(), $verb = 'GET') {


		$url =   "http://" . KitchenAPI_Client::servername . "/BrandService.svc/api/${endpoint}";
		if ( $endpoint == 'login' ) {
			$cparams = array(
				'http' => array(
					'method' => $verb,
					'header' => "Content-Type: application/xml\r\n"
				)
			);

			$cparams['http']['content'] = $params;
			$getparams['format'] = 'json';
			$getparams = http_build_query($getparams);
			$context = stream_context_create($cparams);			
			$url .= '?' . $getparams;
		}
		else {
			$cparams = array(
				'http' => array(
					'method' => $verb,
				)
			);

			$params['token'] = $this->_token;
			$params['format'] = 'json';
			if ($params !== null) {
				$params = http_build_query($params);
				if ($verb == 'POST') {
					$cparams['http']['content'] = $params;
				} else {
					$url .= '?' . $params;
				}
			}
		
			$context = stream_context_create($cparams);
		}
		// print "<pre>URL => $endpoint</pre>\n";
		$fp = fopen($url, 'rb', false, $context);
		if (!$fp) {
			$res = false;

		} else {
			$res = stream_get_contents($fp);
		}
		if ($res === false) {
			// print "<pre>res => $res</pre>\n";
			throw new Exception("$verb $url failed: $php_errormsg");
		}
	
		$r = json_decode($res);
		if ($r === null) {

			throw new Exception("failed to decode $res as json");
		}

		return $r;
	}


    private function _login_impl($userkey, $apikey) {
        if ( $userkey ) {
            $this->_userkey = $userkey;
		}
        if ( $apikey ) {
            $this->_apikey = $apikey;
		}

        if ( file_exists($this->_token_cache_fn) ) {
            // echo "use cached token";
            $this->_token = file_get_contents($this->_token_cache_fn);
		}
        else {
            // echo "login for real";
            $login_xml = "<login><userkey>{$this->_userkey}</userkey><apikey>{$this->_apikey}</apikey></login>";
  
  			$data = array();          
            $data = $this->_call("login", $params=$login_xml, $verb='POST');

            if ( $data->{"status"} == "OK" ) {
                $this->_token = $data->{"results"}->{"token"};
				file_put_contents($this->_token_cache_fn, $this->_token);
			}
            else {
            	throw new Exception(var_export($data, true));
            }
		}
	}
	
    public function login($userkey='', $apikey='') {
        foreach (range(0, 3) as $trycount) {
            $this->_login_impl($userkey, $apikey);

            // do a simple search to make sure the token is valid
            try {
                $this->searchByCategory();
                return TRUE;
			}
			catch(Exception $e) {
                # remove the cached token and try logging in again
                if ( file_exists($this->_token_cache_fn) ) {
                	unlink($this->_token_cache_fn);
                }
			}
		}
        throw new Exception("unable to login, retries failed");
	}

    public function  logout() {
        return $this->_call("logout/" . $this->_token, array());
	}
	
    public function searchByCategory($category=1, $start=NULL, $total=NULL) {
		$params = array('category' => $category);
		if ( $start ) {
            $params["start"] = $start;
		}
		
		if ( $total ) {
            $params["total"] = $total;
		}
        return $this->_call("search", $params);
	}

    public function searchByIngredient($ingredient, $start=NULL, $total=NULL) {
        $params = array("ingredient" => $ingredient);
		if ( $start ) {
            $params["start"] = $start;
		}
		
		if ( $total ) {
            $params["total"] = $total;
		}
        return $this->_call("search", $params);
	}

    public function searchByIngredients($ingredients, $start=NULL, $total=NULL) {
        $ingredients_enc = array();
        foreach( $ingredients as $ingredient ) {
        	$ingredients_enc.append(urlencode($ingredient));
		}

        $params = array("ingredient" => join('|', $ingredients_enc));
		if ( $start ) {
            $params["start"] = $start;
		}
		
		if ( $total ) {
            $params["total"] = $total;
		}
        return $this->_call("search", $params);
	}
	
    public function getRecipe($recipe_id) {
        return $this->_call("recipe/" . $recipe_id, array());
	}

    public function getRecipes($recipe_ids) {
        return $this->_call("recipe/" . join('|', $recipe_ids), array());
	}

    public function getRecipeReviews($recipe_id) {
        return $this->_call("recipereviews/" . $recipe_id, array());
	}
	
    public function getRecipeNutrition($recipe_id) {
        return $this->_call("recipenutrition/" . $recipe_id, array());
	}
	
    public function getRecipeExtended($recipe_id) {
        return $this->_call("recipeextended/" . $recipe_id, array());
	}
	
    public function shoppingListAdd($recipe_id, $customer_id) {
        return $this->_call("shoppinglist/add/" . $recipe_id . '/' . $customer_id, array());
	}
	
    public function shoppingListRemove($recipe_id, $customer_id) {
        return $this->_call("shoppinglist/remove/" . $recipe_id . '/' . $customer_id, array());
	}
	
    public function shoppingListView($customer_id) {
        return $this->_call("shoppinglist/view/" . $customer_id, array());
	}
	
    public function shoppingListEmpty($customer_id) {
        return $this->_call("shoppinglist/empty/" . $customer_id, array());
	}


} // class KitchenAPI_Client
?>
