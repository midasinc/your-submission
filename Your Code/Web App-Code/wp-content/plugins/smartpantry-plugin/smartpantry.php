<?php
/**
 * @package smartpantry_plugin
 * @version 1.0
 */
/*
Plugin Name: smartpantry plugin
Plugin URI: http://www.smartpantry.com
Description: smartpantry plugin
Author: caprica llc
Version: 1.0
Author URI: http://hack.capricapublishing.com
*/


// This just echoes the chosen line, we'll position it later
function smartpantry_admin() {
    ;
    echo "<p id='dolly'>smartpantry</p>";
}

// Now we set that function up to execute when the admin_notices action is called
add_action( 'admin_notices', 'smartpantry_admin' );

// We need some CSS to position the paragraph
function smartpantry_css() {
    // This makes sure that the positioning is also good for right-to-left languages
    $x = is_rtl() ? 'left' : 'right';

    echo "
    <style type='text/css'>
    #dolly {
        float: $x;
        padding-$x: 15px;
        padding-top: 5px;        
        margin: 0;
        font-size: 11px;
    }
    </style>
    ";
}

add_action( 'admin_head', 'smartpantry_css' );

if ( ! class_exists( 'smartpantry' ) ) {

class smartpantry {
    /**
    * @var string
    */
    var $version = '0.0.1';

	var $cart;
	
    /**
     * Constructor.
     *
     * @access public
     * @return void
     */
    function __construct() {

        // Start a PHP session, if not yet started
        if ( ! session_id() )
            session_start();

        // Define version constant
        define( 'smartpantry_VERSION', $this->version );

        $this->includes();
		$this->apiclient = new KitchenAPI_Client();
		
		add_action('init', array( &$this, 'init'), 0);
		

		
		do_action('smartpantry_loaded');
    }

    /**
     * Include required core files.
     *
     * @access public
     * @return void
     */
    function includes() {
        // if ( is_admin() ) $this->admin_includes();
        include('kitchenapi.class.php');
		//include('cart.class.php');
		include('shortcodes.php');
		include('hooks.php');
	}

    function apiclient() {
        return $this->apiclient;
    }
	var $apiclient;
	
    /**
     * Get the plugin url.
     *
     * @access public
     * @return string
     */
    function plugin_url() {
        if ( $this->plugin_url ) return $this->plugin_url;
        return $this->plugin_url = plugins_url( basename( plugin_dir_path(__FILE__) ), basename( __FILE__ ) );
    }
	var $plugin_url;


    /**
     * Get the plugin path.
     *
     * @access public
     * @return string
     */
    function plugin_path() {
        if ( $this->plugin_path ) return $this->plugin_path;

        return $this->plugin_path = untrailingslashit( plugin_dir_path( __FILE__ ) );
    }
	var $plugin_path;



	function init() {
		//if ( ! is_admin() ) {
		//	$this->cart = new Cart();
		//}

	}
	
	function add_recipe_to_cart($recipe_id) {
		if ( isset( $_SESSION['smartpantry_cart'] ) && is_array( $_SESSION['smartpantry_cart'] )) {
	    	array_push($_SESSION['smartpantry_cart'], $recipe_id);
		}
		else {
			$_SESSION['smartpantry_cart'] = array($recipe_id);
		}
		
        update_user_meta( get_current_user_id(), '_smartpantry_persistent_cart', array(
            'smartpantry_cart' => $_SESSION['smartpantry_cart'],
        ));
		return $_SESSION['smartpantry_cart'];
	}

	function remove_recipe_to_cart($recipe_id) {
		if ( isset( $_SESSION['smartpantry_cart'] ) && is_array( $_SESSION['smartpantry_cart'] )) {
	    	$index = array_search($recipe_id, $_SESSION['smartpantry_cart']);
	    	unset($_SESSION['smartpantry_cart'][$index]);
		}
		
        update_user_meta( get_current_user_id(), '_smartpantry_persistent_cart', array(
            'smartpantry_cart' => $_SESSION['smartpantry_cart'],
        ));
		return $_SESSION['smartpantry_cart'];
	}
	
	function empty_cart() {
		$_SESSION['smartpantry_cart'] = array();
		
        update_user_meta( get_current_user_id(), '_smartpantry_persistent_cart', array(
            'smartpantry_cart' => $_SESSION['smartpantry_cart'],
        ));
		return $_SESSION['smartpantry_cart'];
	}

    /**
     * Our debug log writing function
     */
    function debug($message) {
        if (WP_DEBUG === true) {
            if (is_array($message) || is_object($message)) {
                error_log(print_r($message, true));
            } else {
                error_log($message);
            }
        }
    }
} // class smartpantry

/**
 * Init global classes
 */
$GLOBALS['smartpantry'] = new smartpantry();


} // if ( ! class_exists( 'smartpantry' ) )

?>
