<?php

function addcart_shortcode( $attrs, $content = null ) {
	$custom_fields = get_post_custom();
	// var_dump($custom_fields);
	if (array_key_exists('smartpantry_recipe_id', $custom_fields)) {
    	global $smartpantry;
		$recipe_id = $custom_fields['smartpantry_recipe_id'][0];
		$post_url = add_query_arg('add-to-cart', $recipe_id);
		$plugin_url = $smartpantry->plugin_url();

		return <<< EOFHTML
		<link rel="stylesheet" type="text/css" media="all" href="$plugin_url/style.css">
		<div class="tagcloud">
		<form id="recipe_id_form" method="POST" action="">
			<input name="recipe_id" type="hidden" value="$recipe_id"/>
			<a id="add_to_cart" href="javascript:void(0);" class="tag-link-18" title="50 topics" style="font-size: 8pt;">Add to cart</a>
		</form>
		</div>
		<script type="text/javascript">
			jQuery(document).ready(function($) {
				$('a#add_to_cart').click(function() {
					$.ajax({
					  type: "GET",
					  url: "$post_url",
					  data: {},
					  //dataType: 'json',
					  success: function(data, textStatus, jqXHR) {
					  },
					})/*.fail(function() {
						alert("Error: unable to add to cart!");
					})*/;
					
				});
			});

		</script>
EOFHTML;
	}
	else {
		return '';
	}
}
add_shortcode( 'smartpantry_addcart', 'addcart_shortcode' );



function showcart_shortcode( $attrs, $content = null ) {
    global $smartpantry;
	global $current_user;

	$current_user = wp_get_current_user();
	$custom_fields = get_post_custom();
	$apiclient = $smartpantry->apiclient();
	if ( isset( $_SESSION['smartpantry_cart'] ) && is_array( $_SESSION['smartpantry_cart'] )) {
		try {
			$apiclient->login();
		}
		catch (Exception $e ) {
			return <<< EOFHTML
				<div class="error">
					kitchenapi login failed
				</div>
EOFHTML;
		}

		$customer_id = '' . $apiclient->getToken() . '' . $current_user->ID;

		$html =<<< EOFHTML
<style>
.recipeBoxPage .listViewWrapper .recRecipePhotoWrapper {
	margin-right: 13px;
}

.recRecipePhotoWrapper {
	float: left;
	padding: 0 10px 15px;
	width: 220px;
}



.recipeDescWrapper {
	float: left;
	width: 390px;
}

.recRecipePhotoWrapper .recRecipePhoto {
	position: relative;
	border-radius: 15px;
	-moz-border-radius: 15px;
	-webkit-border-radius: 15px;
}

.recRecipePhoto a .recPic {
	background-repeat:  no-repeat;
	background-size:220px 151px;
	width: 220px; height: 151px;
	border-radius: 15px;
	-moz-border-radius: 15px;
	-webkit-border-radius: 15px;
}

.end-of-cart {
	height: 32px;
}        

.remove-from-cart-wrapper {
	float: right;
	background-color: transparent;
	border: 1px solid black;
	display: inline-block;
	border: none;
	width: 48px;
	height: 48px;
}

</style>
EOFHTML;

		$recipes = array();
		$postids = array();
		foreach( $_SESSION['smartpantry_cart'] as $recipe_id ) {
			$args = array(
				'post_type' => 'post',
				'orderby' => 'title',
				'order' => 'ASC',
				'meta_query' => array(
					array('key' => 'smartpantry_recipe_id', 'value' => "$recipe_id")
				)
			);
			$queryObject = new WP_Query($args);
			if ($queryObject->have_posts()) {
				while ($queryObject->have_posts()) {
					$custom_fields = get_post_custom( $queryObject->post->ID );
					// $permalink = get_permalink( $queryObject->post->ID );
					if ( array_key_exists('smartpantry_recipe_data', $custom_fields)) {
						$recipe = json_decode('' . $custom_fields['smartpantry_recipe_data'][0] );
						$postids['' . $recipe->{'recipe_id'}] = $queryObject->post->ID;
						array_push($recipes,  $recipe);
					}
					
					$queryObject->next_post();
				}
			}
		}
		
		try {
			$apiclient->shoppingListEmpty($customer_id);
		}
		catch(Exception $e) {
			// ignored
		}
		
		$recipes_seen = array();
		
		$listid = $customer_id;
		$empty_url = add_query_arg('empty-cart', $listid);

		foreach( $recipes as $recipe ) {
//			echo "<pre>". var_dump($recipe) . "</pre>";
			$recipe_id = $recipe->{"recipe_id"};
			
			// api fails if recipe is added twice, so let's prevent that
			if (in_array($recipe_id, $recipes_seen)) {
				continue;
			}
			
			array_push($recipes_seen, $recipe_id);
			
			$remove_url = add_query_arg('remove-from-cart', $recipe_id);

			$returned = $apiclient->shoppingListAdd($recipe_id, $listid);
			$listid = $returned->{"results"}->{"listid"};

			$postid = $postids['' .  $recipe->{'recipe_id'}];
			$permlink = get_permalink( $postid );
			$name = $recipe->{"name"};
			$description = $recipe->{'description'};
			$thumbimg = $recipe->{'thumbimg'};
			$bigimg = get_the_post_thumbnail( $postid ); // $recipe->{'bigimg'};
			
			$bigimg = '';
			if ( function_exists('has_post_thumbnail') && has_post_thumbnail( $postid ) ) {
				$thumbnail = wp_get_attachment_image_src( get_post_thumbnail_id($postid), full );
				if ( $thumbnail[0] ) {
					$bigimg = $thumbnail[0];
				}
			}

			
			$rating = $recipe->{'rating'};
			$rating_width = intval((floatval($rating) / 5) * 100);
			$html .= <<< EOFHTML
			<div class="recipeResultItem clearfix">
				<div class="remove-from-cart-wrapper">

					<form id="remove_from_cart_form" method="POST" action="$remove_url">
						<input name="recipe_id" type="hidden" value="$recipe_id"/>
						<a href="javascript:void(0);" onclick="jQuery(this).closest('form').submit();" class="remove-from-cart-btn">X</a>
					</form>
				</div>
				
				<div class="recRecipePhotoWrapper">
	                <div class="recRecipePhoto">
	                    <a role="link" title="$name" href="$permlink">
	                        <div class="recPic" style="background-image: url($bigimg);" alt="Recipe"  role="presentation">
	                        </div>
	                    </a>
	                </div>
	            </div>

	            <div class="recipeDescWrapper">
	                <p class="entry-title">
	                	<a href="$permlink" title="$name" role="link" >$name</a>
					</p>
				
					<div class="ratingblock ">
						<div class="ratingheader "></div>
						<div class="ratingstars ">
							<div id="article_rater_149" class="ratepost gdsr-oxygen gdsr-size-12">
								<div class="starsbar gdsr-size-20">
									<div class="gdouter gdheight" style="width: 100px;">
										<div id="gdr_vote_a149" style="width: ${rating_width}px;" class="gdinner gdheight"></div>
										<div id="gdr_stars_a149" class="gdsr_rating_as">
											<a title="5 / 5" class="s5" rel="nofollow"></a>
											<a title="4 / 5" class="s4" rel="nofollow"></a>
											<a title="3 / 5" class="s3" rel="nofollow"></a>
											<a title="2 / 5" class="s2" rel="nofollow"></a>
											<a title="1 / 5" class="s1" rel="nofollow"></a>
										</div>
									</div>
								</div>
							</div>
						</div>
						<div class="ratingtext">
							<div id="gdr_text_a149">
								Rating: <strong>$rating</strong> out of <strong>5</strong>
							</div>
						</div>
					</div>
	                <p class="entry-content">
	                    $description
	                </p>
	                <br>
	                
	            </div>


	        </div>

EOFHTML;
	 	}
	 	
		
		$returned = $apiclient->shoppingListView( $listid );
		$list_items = $returned->{"results"}->{"list"};

	 	$shoppinglist_html =<<<EOFHTML
	 	<div class="tagcloud" style="width: 256px; display: block;">
		<form id="empty_form" method="POST" action="$empty_url">
			<a id="empty_cart" href="javascript:void(0);" onclick="jQuery(this).closest('form').submit();"  class="tag-link-18" title="50 topics" style="font-size: 8pt;">Empty cart</a>
		</form>
	 	</div>
	 	<div style="width: 100%; height: 64px;">
	 	</div>
EOFHTML;

		$shoppinglist_html .= '<div class="widget widget_categories"><h3>Shopping List</h3><ul>';
		
		foreach($list_items as $li) {
			$litxt = '<span class="quantity">' . $li->{'quantity'} . '</span> <span class="item">' . $li->{'item'} . '</span>'; 
			$shoppinglist_html .= "<li class='cat-item'>$litxt</li>";
		}
		
		$shoppinglist_html .= '</ul></div>';
		
		return <<< EOFHTML
		<div class="cart">

		$html

		</div>
		
		$shoppinglist_html

		<div class="end-of-cart"></div>
EOFHTML;
	}
	else {
		return <<< EOFHTML
		<div class="cart empty">Your cart is empty</div>
EOFHTML;
	}


}
add_shortcode( 'smartpantry_showcart', 'showcart_shortcode' );


?>