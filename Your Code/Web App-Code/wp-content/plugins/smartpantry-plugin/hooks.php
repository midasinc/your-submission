<?php

/**
 * Cart Actions
 *
 */

function smartpantry_add_to_cart_action( $url = false ) {
    global $smartpantry;

    if ( empty( $_REQUEST['add-to-cart'] ) ) return;

	$added_to_cart      = false;

    // Get product ID to add and quantity
    $recipe_id     = (int) $_REQUEST['add-to-cart'];

    // Add the product to the cart
    if ($smartpantry->add_recipe_to_cart($_REQUEST['add-to-cart'])) {
    	$added_to_cart = true;
    }
}

add_action( 'init', 'smartpantry_add_to_cart_action' );


function smartpantry_remove_from_cart_action( $url = false ) {
    global $smartpantry;

    if ( empty( $_REQUEST['remove-from-cart'] ) ) return;

	$removed_from_cart      = false;

    // Get product ID to add and quantity
    $recipe_id     = (int) $_REQUEST['remove-from-cart'];

    // Add the product to the cart
    if ($smartpantry->remove_recipe_to_cart($_REQUEST['remove-from-cart'])) {
    	$removed_from_cart = true;
    }
}
add_action( 'init', 'smartpantry_remove_from_cart_action' );

function smartpantry_empty_cart_action( $url = false ) {
    global $smartpantry;

    if ( empty( $_REQUEST['empty-cart'] ) ) return;

	$emptied_cart      = false;

    // Add the product to the cart
    if ($smartpantry->empty_cart()) {
    	$emptied_cart = true;
    }
}
add_action( 'init', 'smartpantry_empty_cart_action' );


function smartpantry_load_persistent_cart( $user_login, $user ) {
    global $smartpantry;

    $saved_cart = get_user_meta( $user->ID, '_smartpantry_persistent_cart', true );

    if ($saved_cart) {
        if (!isset($_SESSION['smartpantry_cart']) || !is_array($_SESSION['smartpantry_cart']) || sizeof($_SESSION['smartpantry_cart'])==0) {

            $_SESSION['smartpantry_cart'] = $saved_cart['smartpantry_cart'];

        }
    }
}
 
add_action( 'wp_login', 'smartpantry_load_persistent_cart', 1, 2 );

?>